# NGINX file
 This repository contains default configuration file NGINX
 ## Getting Started
 ### Prerequisites
 Need install **nginx** on your system
 
 For Debian OS run:
 ```
 apt-get install nginx
 ```
 For RedHat OS run:
 ```
 yum install nginx
 ```
 ### Installing
To install,  just copy file **nginx.conf** to configuration directory **nginx** and reload service:
```
$ sudo cp ~/nginx.conf /etc/nginx/nginx.conf
$ sudo nginx -s reload
```
## Authors

* **Max Sakalyuk** -  [sand-max](https://gitlab.com/sand-max)

ddddddddd